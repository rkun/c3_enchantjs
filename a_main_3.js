enchant();

window.onload = function(){
  var itokawas = [];
  var bear;
  var bearMoveStep = 8;

  function generateItokawa(x) {
    var itokawa = new Sprite(64, 64);
    itokawa.image = game.assets["itokawa.png"];
    itokawa.x = x;
    itokawa.y = 0;
    itokawa.frame = 1;

    itokawas.push(itokawa);
    itokawa.intersect(bear, function(){
      console.log("Wow");
    });

    game.rootScene.addChild(itokawa);
  }
    var game = new Core(320, 320);
    game.fps = 15;
    game.preload("chara1.png");
    game.preload("itokawa.png");
    game.onload = function(){
        bear = new Sprite(32, 32);
        bear.image = game.assets["chara1.png"];
        bear.x = 0;
        bear.y = 278;
        bear.frame = 5;
        game.rootScene.addChild(bear);


        game.rootScene.addEventListener("enterframe", function(){
            this.frame = this.age % 2 + 6;

            if(this.age % (15*2) ==  0) generateItokawa(Math.random() * 320);
            itokawas.forEach(function(itokawa){
              itokawa.y += 2;
              if(itokawa.intersect(bear)) game.rootScene.removeChild(bear);
            })

            if(game.input.left && bear.x >= 0) bear.x -= bearMoveStep;
            if(game.input.right && bear.x <= game.width - bear.width) bear.x += bearMoveStep;

        });

    };
    game.start();
};
