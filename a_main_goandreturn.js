enchant();

window.onload = function(){
    var game = new Core(320, 320);
    var go_right = 1;
    game.fps = 15;
    game.preload("chara1.png");
    game.onload = function(){
        var bear = new Sprite(32, 32);
        bear.image = game.assets["chara1.png"];
        bear.x = 0;
        bear.y = 0;
        bear.frame = 5;
        game.rootScene.addChild(bear);

        bear.addEventListener("enterframe", function(){
            if(go_right){
              this.x += 1;
              if(this.x > game.width) go_right = 0;
            }else{
              this.x -= 1;
              if(this.x < 0) go_right = 1;
            }
            this.frame = this.age % 2 + 6;
        });

        bear.addEventListener("touchstart", function(){
            game.rootScene.removeChild(bear);
        });
    };
    game.start();
};
